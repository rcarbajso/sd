/* DMUTEX (2009) Sistemas Operativos Distribuidos
 * C�digo de Apoyo
 *
 * ESTE C�DIGO DEBE COMPLETARLO EL ALUMNO:
 *    - Para desarrollar las funciones de mensajes, reloj y
 *      gesti�n del bucle de tareas se recomienda la implementaci�n
 *      de las mismas en diferentes ficheros.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <arpa/inet.h>/* inet_* */
#include <netinet/in.h>/* IP*, sockaddr_in, ntoh?, hton?, etc. */
#include <netinet/tcp.h>/* TCPOPT_*, etc. */
#include <sys/socket.h>/* socket */
#include <sys/types.h>/* socket */
#include <sys/un.h>/* sockaddr_un */
#include <netdb.h>/* gethostbyname */
#include <stdio.h>
#include <unistd.h>
#define LOCALHOST "127.0.0.1"
#define MSG 0
#define LOCK 1

typedef struct elemento{
    size_t puerto;
    char proceso [80];
}elemento;

typedef struct message
{
  size_t* reloj;
  char sec[80];
  size_t tipo;
  char proc_name [80];
}message;

int puerto_udp, n_relojes, indexReloj;
elemento* hash;
int* reloj, *puertos;
char* procesos;
struct sockaddr_in conect(int port){
  struct hostent * hp;
  struct sockaddr_in s_ain;
  hp = gethostbyname(LOCALHOST);
  bzero((char *)&s_ain, sizeof(s_ain));
  s_ain.sin_family = AF_INET;
  memcpy (&(s_ain.sin_addr), hp->h_addr_list[0], hp->h_length);
  s_ain.sin_port = htons(port);
  return s_ain;
}
int getPort(char* name){
  int i;
  for(i = 0; i < n_relojes; i++){
    if(!strcmp(hash[i].proceso,name))
      return hash[i].puerto;
  }
  return -1;
}
void comp(size_t* reloj_msg){
  int i;
  for(i = 0; i < n_relojes; i++){
    if(reloj[i] < reloj_msg[i]){
      reloj[i]=reloj_msg[i];
    }
  }
}
int main(int argc, char* argv[])
{
  hash = (elemento*) malloc(sizeof(elemento) * 4);
  int sd, port;
  socklen_t size;
  struct sockaddr_in s_ain, c_ain;
  char line[80],proc[80],op[80];
  if(argc<2)
  {
    fprintf(stderr,"Uso: proceso <ID>\n");
    return 1;
  }

  /* Establece el modo buffer de entrada/salida a l�nea */
  setvbuf(stdout,(char*)malloc(sizeof(char)*80),_IOLBF,80);
  setvbuf(stdin,(char*)malloc(sizeof(char)*80),_IOLBF,80);

  sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  bzero((char *)&s_ain, sizeof(s_ain));
  s_ain.sin_family = AF_INET;
  s_ain.sin_addr.s_addr = INADDR_ANY;
  s_ain.sin_port = htons(0);
  bind(sd, (struct sockaddr *)&s_ain, sizeof(s_ain));
  size = sizeof(c_ain);
  getsockname(sd,(struct sockaddr *)&s_ain,&size);
  puerto_udp=ntohs(s_ain.sin_port); 
  fprintf(stdout,"%s: %d\n",argv[1],puerto_udp);
  n_relojes=0;
  for(;fgets(line,80,stdin);)
  {
    if(!strcmp(line,"START\n"))
      break;

    sscanf(line,"%[^:]: %d",proc,&port);
    if(!strcmp(proc,argv[1])){
       indexReloj = n_relojes; 
        n_relojes++;
    }

    else{
      hash[n_relojes].puerto = port;
      strcpy(hash[n_relojes++].proceso,proc);
    }
  }
  reloj = calloc(n_relojes , sizeof(int));
  
  for(;fgets(line,80,stdin);)
  {
    if(!strcmp(line,"FINISH\n")){
      break;
    }
    else if(!strcmp(line,"EVENT\n")){
      reloj[indexReloj]++;
      printf("%s: TICK\n",argv[1]);
    }
    else if(!strcmp(line,"GETCLOCK\n"))
    {
      printf("%s: LC[",argv[1]);
      int i = 0;
      for(; i < n_relojes - 1; i++)
        printf("%d,",reloj[i]);
      printf("%d]\n",reloj[i]);
    }
    else if(!strcmp(line,"RECEIVE\n"))
    {

      message * m = malloc(sizeof(message));
      char* tipo;
      char buffer [256] = {0};
      int response = recvfrom(sd, &buffer, sizeof(buffer), 0, (struct sockaddr *)&c_ain, &size);
      int offset = 0;
      memcpy(&m->tipo,buffer + offset,sizeof(int));
      offset+=sizeof(int);
      int i;
      m->reloj = malloc(sizeof(int) * n_relojes);
      for(i = 0; i < n_relojes; i++){
        memcpy(&(m->reloj[i]),buffer + offset, sizeof(int));
        offset+= sizeof(int);
      }
      memcpy(m->proc_name, buffer + offset, 80);
      offset+=80;
      if(m->tipo)
        tipo = "LOCK";
      else
        tipo = "MSG"; 
      printf("%s: RECEIVE(%s,%s)\n",argv[1],tipo,m->proc_name);
      comp(m->reloj);
      printf("%s: TICK\n",argv[1]);
      reloj[indexReloj]++;
    }
    else{
      sscanf(line,"%s %s",proc,op);
      if(!strcmp(proc,"MESSAGETO")){
        reloj[indexReloj]++;
        printf("%s: TICK\n",argv[1]);
        int res;
        if(!strcmp(op,argv[1]))
          res = puerto_udp;
        else{
          res = getPort(op);
        }
        if(res != -1){
          c_ain = conect(res);
          message* m = malloc(sizeof(message));
          int i;
          for (i = 0; i<n_relojes;i++){
            m -> reloj[i] = reloj[i];
          }
          m->tipo = MSG;
          strcpy(m->proc_name,argv[1]);
          char buffer[256];
          size = 80 + sizeof(size_t) * (n_relojes + 1);
          int offset=0;
          memcpy(buffer + offset,&m->tipo,sizeof(int));
          offset+=sizeof(int);
          for(i = 0; i < n_relojes; i++){
            memcpy(buffer + offset,&(m->reloj[i]),sizeof(int));
            offset+= sizeof(int);
          }
          memcpy(buffer + offset, argv[1],80);
          offset+=80;
          sendto(sd,&buffer,size,0,(struct sockaddr *)&c_ain,size);
          printf("%s: SEND(MSG,%s)\n",argv[1],op);
        }
      }
      else if (!strcmp(proc,"LOCK")){
        reloj[indexReloj]++;
        printf("%s: TICK\n",argv[1]);
        size = 80 + sizeof(size_t) * (n_relojes + 1) + 80;
        message* m = malloc(sizeof(message));
        int i,offset;
        m->reloj = calloc(n_relojes, sizeof(int));
        for (i = 0; i<n_relojes;i++){
          m -> reloj[i] = reloj[i];
        }
        m->tipo = LOCK;
        char buffer[256];
        memcpy(buffer + offset,&m->tipo,sizeof(int));
        offset+=sizeof(int);
        for(i = 0; i < n_relojes; i++){
          memcpy(buffer + offset,&(m->reloj[i]),sizeof(int));
          offset+= sizeof(int);
        }
        memcpy(buffer + offset, argv[1],80);
        offset+=80;
        memcpy(buffer + offset,op,80);
        for(i = 0;i < n_relojes; i++){
          if(i != indexReloj){
            c_ain = conect(hash[i].puerto);
            printf("%s: SEND(LOCK,%s)\n",argv[1],hash[i].proceso);
            sendto(sd,&buffer,size,0,(struct sockaddr *)&c_ain,size);
            printf("Size: %d\n",size);
          }
        }
      }
    }
  }
  return 0;
}

