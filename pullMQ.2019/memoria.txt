PullMQ.2019 se trata de una implementación de la primera práctica individual de la asignatura de Sistemas Distribuidos.
He implementado todo el servicio broker en el fichero broker/broker.c y el cliente en el archivo libpullMQ/libpullMQ. En libpull hago una conexión al principio de cada operación.
He utilizado comun.c para implementar la conexión de libpull y las estructuras necesarias para el servicio de colas.
He tardado aproximadamente 20 horas en resolver la práctica y he asistido a 2 tutorías con el profesor para resolver dudas.