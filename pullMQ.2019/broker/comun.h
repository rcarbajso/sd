/*
 * Incluya en este fichero todas las definiciones que pueden
 * necesitar compartir el broker y la biblioteca, si es que las hubiera.
 */
#define CREATE 0
#define DESTROY 1
#define PUT 2
#define GET 3
#define GETB 4

typedef struct message{
    void * mensaje;
    int size;
}message;
typedef struct queue{
    char* name;
    int bottom;
    int front;
    int tam; //Tam in B
    message* array;
    int size_msg;
    int tam_sockets;
    int size_sockets;
    int* sockets;
}queue;
typedef struct hashmap{
    queue* mq;
    int size;
    int tam;
}hashmap;

int connectClient();
