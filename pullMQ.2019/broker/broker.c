#include <stdio.h>
#include "comun.h"
#include <arpa/inet.h>
/* inet_* */
#include <netinet/in.h>
/* IP*, sockaddr_in, ntoh?, hton?, etc. */
#include <netinet/tcp.h>
/* TCPOPT_*, etc. */
#include <sys/socket.h>
/* socket */
#include <sys/types.h>
/* socket */
#include <sys/un.h>
/* sockaddr_un */
#include <netdb.h>
/* gethostbyname */
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>

hashmap* MQ;
int putMQ(char* name, void* mensaje, int tam);
int checkMQ(char* name);
int destroyMQ(char* name);
queue* createMQ(char* name);
void push(queue* q, void* element,int tam);
message pull(queue* q);
message get(char* name);
void eliminar(int i);
void getB(char* name);
void block(char* name,int socket);
void unlock(int socket,void* mensaje,int tam);
hashmap* createH(){
    hashmap* aux = (hashmap *) malloc(sizeof(hashmap));
    aux -> mq = (queue*) malloc(sizeof(queue));
    aux -> size = 0;
    aux -> tam = 1;
    return aux;
}
int insertMQ(char* buffer, int tam){
   int i =checkMQ(buffer);
   if(i < 0){
      char* name=malloc(tam+1);
      strcpy(name,buffer);
      queue* q = createMQ(name);
      MQ -> mq[MQ -> size++] = *q;
      MQ -> mq = (queue*) realloc(MQ -> mq,(MQ -> size * sizeof(queue)) + sizeof(queue));
      printf("size: %d\n",MQ->size);
      return 0;
   }
   else{
      printf("Ya existe esa MQ\n");
      return -1;
   }
}
int checkMQ(char * name){
    queue * list = MQ -> mq;
    fprintf(stderr, "checkMQ\n");
    for (int i = 0; i < MQ ->size; i++){
       fprintf(stderr, "checkMQ %d %d\n", strlen((&list[i])->name),strlen(name));
        if (strcmp((&list[i])->name,name) == 0){
            return i;
        }
    }
    return -1;
}
int destroyMQ(char* name){
   int i = checkMQ(name);
   if(i != -1){
      queue* q = malloc(sizeof(queue));
      *q = MQ -> mq[i];
      printf("size_sockets: %d\n",q->size_sockets);
      if(q->size_sockets){
         int res = -1;
         for(int j = 0;j < q ->size_sockets; j++){
            printf("socket: %d\n",q->sockets[j]);
            write(q->sockets[j],&res,sizeof(int));
         }
      }
      eliminar(i);
      return 0;
   }
   else{
      printf("No existe esa cola\n");
      return -1;
   }
}
void eliminar(int i){
   printf("Eliminar %d\n",i);
   queue * list = MQ -> mq;
   int size = MQ -> size;
   for(int j = i;j<size;j++){
      list[j] = list[j+1];
   }
   MQ -> size = --size;
   // MQ -> mq = list;

}
queue* createMQ(char* name){
   queue* aux = (queue *) malloc(sizeof(queue));
   aux -> bottom = aux -> front = 0;
   aux -> array = (message*) malloc(sizeof(message));
   aux -> name = name;
   aux -> size_msg = 0; 
   aux -> sockets = (int*) malloc(sizeof(int));
   aux -> size_sockets = 0;
   aux -> tam_sockets = 100;
   aux -> tam = 0;
   return aux;
}
int putMQ(char* name, void* mensaje, int tam){
   int i = checkMQ(name);
   if (i >= 0){
      queue * list = MQ -> mq;
      queue* q = (&list[i]); 
      if(q -> size_sockets){
         unlock(q -> sockets[0], mensaje, tam);
         int size = q->size_sockets;
         int* sockets = q ->sockets;
         for(int j = i;j<size;j++){
            sockets[j] = sockets[j+1];
         }
         q->size_sockets--;
         // MQ->mq = list;
         return 0;
      }
      else{    
         push(&list[i],mensaje,tam);
         MQ -> mq[i] = list[i];
         return 0; 
      }
     }
   else{
      printf("No existe esa cola\n");
      return -1;
   }
}
void push(queue* q, void* element, int tam){
   message * m = (message*) malloc (sizeof(message));
   m -> size = tam;
   m -> mensaje = malloc(tam);
   m -> mensaje = element;
   q -> tam += m->size + sizeof(void*);
   q -> array = (message*) realloc(q->array, q -> tam);
   q -> array[q -> bottom++] = *m;
   q -> size_msg = q -> size_msg + 1;
   }
message pull(queue* q){
   if(q->size_msg){
      printf("La cola no está vacía\n");
      message* res = (message*) malloc(sizeof(message));
      int front = q ->front;
      res ->size = (&q ->array[front]) -> size;
      res->mensaje = malloc(res ->size);
      res ->mensaje = (&q ->array[front]) -> mensaje;
      (&q ->array[front]) -> mensaje = "\0";
      q ->front++;
      q -> size_msg--;
      q -> tam -= sizeof(res);
      return *res;
   }
   printf("La cola está vacía\n");
   message * m = (message*) malloc (sizeof(message));
   m -> size = 0;
   m -> mensaje = NULL;
   return *m;
}
message get(char* name){
   int i = checkMQ(name);
   if(i >= 0){
      queue * list = MQ -> mq;
      message res = pull(&list[i]);
      MQ -> mq = list;
      return res;
   }
   else{
      printf("No existe esa cola\n");
      message * m = (message*) malloc (sizeof(message));
      m -> size = -1;
      return *m;
   }
}
void block(char* name,int socket){
   queue* list = MQ ->mq;
   int i = checkMQ(name);
   (&list[i]) -> sockets[(&list[i]) -> size_sockets++] = socket;
   (&list[i]) -> sockets = realloc((&list[i]) -> sockets,(&list[i])->size_sockets * sizeof(int) + sizeof(int) );
   printf("Número de sockets en espera: %d\n",(&list[i]) -> size_sockets);
   printf("size_sockets: %d\n",list->size_sockets);
   MQ -> mq = list;
}
void unlock(int socket, void* mensaje, int tam){
   int res = 0;
   write(socket,&res,sizeof(int));
   int size = ntohl(tam);
   write(socket,&size,sizeof(int));
   printf("Tam unlock: %d\n",tam);
   char* c = malloc(tam);
   memcpy(c, mensaje, tam);
   printf("Mensaje unlock: %s\n",c);
   write(socket,c,tam);
}
int main(int argc, char *argv[]){

   if(argc!=2) {
       fprintf(stderr, "Uso: %s puerto\n", argv[0]);
       return 1;
   }
   unsigned int server,client,len;
   struct sockaddr_in s_addr,c_addr;
   MQ = createH();   
   len=sizeof(struct sockaddr_in);
   server=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
   /* Creacion del socket TCP de servicio */
   if(server)
      fprintf(stdout,"SERVIDOR: Creacion del socket TCP: OK\n");
   else{
      fprintf(stdout,"SERVIDOR: Creacion del socket TCP: ERROR\n");
      exit(1);
   }
   bzero((char*)&s_addr,sizeof(struct sockaddr_in));
   s_addr.sin_family=AF_INET;
   s_addr.sin_addr.s_addr=INADDR_ANY;
   s_addr.sin_port=htons(atoi(argv[1]));
   /* Asignacion de la direccion local (del servidor) Puerto TCP*/
   if(bind(server,(struct sockaddr *)&s_addr,sizeof(struct sockaddr_in))==0)
      fprintf(stdout,"SERVIDOR: Asignacion del puerto servidor: OK\n");
   else{
      fprintf(stdout,"SERVIDOR: Asignacion del puerto servidor: ERROR\n");
      exit(1);
   }
   if(listen(server,5)==0)
   /* Aceptamos conexiones por el socket */
      fprintf(stdout,"SERVIDOR: Aceptacion de peticiones: OK\n");
   else{
      fprintf(stdout,"SERVIDOR: Aceptacion de peticiones: ERROR\n");
      exit(1);
   }
   /* Puerto TCP ya disponible */
   if(getsockname(server,(struct sockaddr *)&s_addr,(socklen_t*)&len)<0){
      fprintf(stdout,"SERVIDOR: Puerto TCP reservado: ERROR\n");
      close(server);
      close(server);
      exit(1);
   }
   else{
      fprintf(stdout,"SERVIDOR: Puerto TCP reservado: OK\n");
      }
    while(1){
         int op = 0;
         int tam = 0;
         int res = 0;
         client = accept(server,(struct sockaddr*)&c_addr,&len);
         if(read(client,&tam,sizeof(int)) > 0){
            tam = htonl(tam);
            printf("Tamaño: %d\n",tam);
            char* name = malloc(sizeof(char) * (tam + 1));
            if(read(client,name,tam)>0){
               name[tam] = '\0'; 
               if(read(client,&op,sizeof(int)) > 0){
                  //OPERACIONES
                  printf("Operación: %d\n",op);
                  if(op == CREATE){
                     res = insertMQ(name, tam);
                     write(client,&res,sizeof(int));
                     close(client);
                  }
                  else if(op == DESTROY){
                     res = destroyMQ(name);
                     write(client,&res,sizeof(int));
                     close(client);
                  }
                  if(op == PUT){
                     if(read(client,&tam,sizeof(int))){
                        tam = htonl(tam);
                        printf("Tamaño_put: %d\n",tam);
                        char* mensaje = (char*) malloc(sizeof(char) * tam);
                        if(recv(client,mensaje,tam,MSG_WAITALL)){
                           res = putMQ(name,mensaje,tam);
                           write(client,&res,sizeof(int));
                        }
                     }
                     close(client);
                  }
                  else if(op == GET){
                     message* m = (message*) malloc (sizeof(message));
                     *m = get(name);
                     tam = m -> size;
                     if(tam == -1){
                        res = -1;
                        write(client,&res,sizeof(int));
                     }
                     else{
                        write(client,&res,sizeof(int));
                        printf("Tamaño_get: %d\n",tam);
                        char* mensaje = (char*) malloc(sizeof(char) * tam);
                        memcpy(mensaje,m -> mensaje,tam);
                        int tam_msg = ntohl(tam);
                        write(client,&tam_msg,sizeof(int));
                        if (tam){
                           write(client,mensaje,tam);
                           free(mensaje);
                        }
                     }
                     free(m);
                     close(client);
                  }
                  else if(op==GETB){
                     message* m = (message*) malloc (sizeof(message));
                     printf("Get bloqueante\n");
                     *m = get(name);
                     tam = m -> size;
                     if(tam == -1){
                        res = -1;
                        write(client,&res,sizeof(int));
                     }
                     else{
                        printf("Tamaño_get: %d\n",tam);
                        if(tam == 0){
                           printf("Bloqueando...\n");
                           block(name,client);
                        }
                        else{
                           printf("No bloqueante\n");
                           write(client,&res,sizeof(int));
                           char* mensaje = (char*) malloc(sizeof(char) * tam);
                           memcpy(mensaje,m -> mensaje,tam);
                           int tam_msg = ntohl(tam);
                           write(client,&tam_msg,sizeof(int));
                           write(client,mensaje,tam);
                           free(mensaje);
                        }
                     }
                     free(m);
                  }
         } 
    }
         }
             }
    close(server);
    return 0;
}
         
