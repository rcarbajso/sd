/*
 * Incluya en este fichero todas las implementaciones que pueden
 * necesitar compartir el broker y la biblioteca, si es que las hubiera.
 */
#include "comun.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>   
#include <errno.h>   
#include <stdlib.h>
#include <netdb.h>

int connectClient(){
    struct hostent* host_info;
    int sock_tcp; /* Socket (de tipo TCP) para transmision de datos */
    struct sockaddr_in  dir_tcp_srv;  /* Direccion TCP servidor */
    /* Creacion del socket TCP */
    fprintf(stdout,"CLIENTE:  Creacion del socket TCP: ");
    sock_tcp=socket(PF_INET,SOCK_STREAM,0);
    if(sock_tcp<0)
      {
        fprintf(stdout,"ERROR\n");
        exit(1);
      }
    fprintf(stdout,"OK\n");
    /* Nos conectamos con el servidor */
    bzero((char*)&dir_tcp_srv,sizeof(struct sockaddr_in)); /* Pone a 0 
    							    la estructura */
    dir_tcp_srv.sin_family=AF_INET;
    host_info = gethostbyname(getenv("BROKER_HOST"));
    memcpy(&dir_tcp_srv.sin_addr.s_addr,host_info->h_addr_list[0],host_info->h_length);
    dir_tcp_srv.sin_port=htons(atoi(getenv("BROKER_PORT")));
    fprintf(stdout,"CLIENTE:  Conexion al puerto servidor: ");
    if(connect(sock_tcp,
    	     (struct sockaddr*)&dir_tcp_srv,
    	     sizeof(struct sockaddr_in))<0)
      {
        fprintf(stdout,"ERROR %d\n",errno);
        close(sock_tcp); exit(1);
      }
    fprintf(stdout,"OK\n");
    return(sock_tcp);
}

