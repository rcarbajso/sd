#include <stdio.h>
#include "comun.h"
#include <arpa/inet.h>
/* inet_* */
#include <netinet/in.h>
/* IP*, sockaddr_in, ntoh?, hton?, etc. */
#include <netinet/tcp.h>
/* TCPOPT_*, etc. */
#include <sys/socket.h>
/* socket */
#include <sys/types.h>
/* socket */
#include <sys/un.h>
/* sockaddr_un */
#include <netdb.h>
/* gethostbyname */
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include "pullMQ.h"
#include <errno.h> 

int createMQ(const char *cola) {
	int res;
	int op = CREATE;
	int tam = ntohl(strlen(cola));
	printf("Tam: %d\n",tam);
	int socket = connectClient();
	write(socket,&tam,sizeof(int));
	tam = htonl(tam);
	write(socket,cola,tam);
	write(socket,&op,sizeof(int));
	read(socket,&res,sizeof(int));
	close(socket);
	return res;
	}
int destroyMQ(const char *cola){
	int res;
	int op = DESTROY;
	int tam = ntohl(strlen(cola));
	int socket = connectClient();
	write(socket,&tam,sizeof(int));
	tam = htonl(tam);
	write(socket,cola,tam);
	write(socket,&op,sizeof(int));
	read(socket,&res,sizeof(int));
	close(socket);
	return res;
}
int put(const char *cola, const void *mensaje, size_t tam){
	int op = PUT;
	int res = 0;
	char* c = malloc(tam); 
	int socket = connectClient();
	printf("Op: %d\n",op);
	memcpy(c, mensaje, tam);
	printf("Tam_put: %d\n",tam);
	int tam_msg = ntohl(tam);
	int tam_cola = ntohl(strlen(cola)) ;
	write(socket,(&tam_cola),sizeof(int));
	tam_cola = htonl(tam_cola);
	write(socket,cola,tam_cola);
	write(socket,&op,sizeof(int));
	write(socket,&tam_msg,sizeof(int));
	write(socket,c,tam);
	read(socket,&res,sizeof(int));
	printf("RESULTADO: %d\n",res);
	free(c); 
	close(socket);
	return res; 
}
int get(const char *cola, void **mensaje, size_t *tam, bool blocking){
	printf("Geet\n");
	printf("Block: %d\n",blocking);
	int op = (blocking) ?GETB:GET;
	printf("Op: %d\n",op);
	int socket = connectClient();
	int tam_cola = ntohl(strlen(cola)) ;
	int res;
	write(socket,&tam_cola,sizeof(int));
	tam_cola = htonl(tam_cola);
	write(socket,cola,tam_cola);
	write(socket,&op,sizeof(int));
	recv(socket,&res,sizeof(int),MSG_WAITALL);
	printf("Desbloqueando...%d\n",res);
	if(res == 0){
		int size;
		read(socket,&size,sizeof(int));
		*tam = htonl(size);
		printf("Tamaño_get: %d\n",*tam);
		if(*tam){
			char* m = (char*) malloc(sizeof(char) * (*tam));
			recv(socket,m,*tam,MSG_WAITALL);
			//read(socket,m,size);
			printf("Mensaje_get: %s\n",m);
			*mensaje=m;
		}
		}
	close(socket);
	return res;
}
